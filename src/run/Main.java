/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package run;

import ModeloDatos.Persona;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author asillero
 */
public class Main {

    public static void main(String[] args) {

        Persona p1 = new Persona("Luis", "Córdoba");
        Persona p2 = new Persona("Juan", "Sevilla");
        Persona p3 = new Persona("Pepe", "Málaga");
        Persona px;

        Session ss = NewHibernateUtil.getSessionFactory().openSession();

        ss.beginTransaction();
        ss.persist(p1);
        ss.persist(p2);
        ss.save(p3);
        
                p2.setNombre("ibuprofeno");//la modificación se realiza tambien en la base de datos mientras exista la sesión


        ss.getTransaction().commit();
        ss.close();

        ss = NewHibernateUtil.getSessionFactory().openSession();

        px = (Persona) ss.get(Persona.class, (int) 1); //recupera un objeto de la base de datos
        System.out.println("La Persona con id 1 es: " + px.getNombre());
        ss.beginTransaction();
        ss.update(px);

        px.setNombre("Nombre Actualizado");
        
        ss.getTransaction().commit();
        
        ss.evict(px);//Libero el objeto px


        System.out.println("Recuperar lista de personas");
        Query q = ss.createQuery("from Usuarios");
        //se puede limitar el resultado con q.fetchsize(10);

        //List<Persona> lista = q.list(); //o directamente:
        Iterator<Persona> it = q.iterate();

        while (it.hasNext()) {
            px = (Persona) it.next();
            System.out.println(px.getNombre() + " : " + px.getDireccion());
        }
        
        
        System.out.println("Recuperar lista de personas de Málaga");
//        q = ss.createQuery("from Usuarios as u where u.direccion=?").setString(0, "Málaga");
        q = ss.createQuery("from Usuarios as u where u.direccion=\'Málaga\'");
        //se puede limitar el resultado con q.fetchsize(10);

        //List<Persona> lista = q.list(); //o directamente:
         it = q.iterate();

        while (it.hasNext()) {
            px = (Persona) it.next();
            System.out.println(px.getNombre() + " : " + px.getDireccion());
        }
        
                System.out.println("Recuperar lista de personas de Málaga que se llamen Pepe");
//        q = ss.createQuery("from Usuarios as u where u.direccion=? and u.nombre=?").setString(0, "Málaga").setString(1, "Pepe");
        q = ss.createQuery("from Usuarios as u where u.direccion=:dir and u.nombre=:nom").setString("dir", "Málaga").setString("nom", "Pepe"); //los metodos q.set... se pueden llamar en lineas consecutivas
//        q = ss.createQuery("from Usuarios as u where u.direccion=\'Málaga\' and u.nombre=\'Pepe\'");
        //se puede limitar el resultado con q.fetchsize(10);

        //List<Persona> lista = q.list(); //o directamente:
         it = q.iterate();

        while (it.hasNext()) {
            px = (Persona) it.next();
            System.out.println(px.getNombre() + " : " + px.getDireccion());
        }
        
        
        System.out.println("Borrar una la persona con id=4");
        px=(Persona) ss.createQuery("from Usuarios as u where u.id=?").setInteger(0, 4).uniqueResult();
        ss.beginTransaction();
        ss.delete(px);
        ss.getTransaction().commit();
        ss.close();
        
        NewHibernateUtil.close();

    }

}
